/**
 * Chip-8 emulator
 *
 * Copyright (C) 2022 cosimone64
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * You should have received a copy of the GNU AGPLv3 with this software,
 * if not, please visit <https://www.gnu.org/licenses/>
 */

#define _POSIX_SOURCE
#define _POSIX_C_SOURCE 200112L
#define _XOPEN_SOURCE 500

#include <SDL2/SDL.h>
#include <math.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <time.h>
#include <unistd.h>

#define LICENSE_INFO                                                          \
  "Copyright (C) 2018-2021, cosimone64\n\n"                                   \
  "This program is free software: you can redistribute it and/or modify\n"    \
  "it under the terms of the GNU Affero General Public License as\n"          \
  "published by the Free Software Foundation, either version 3 of the\n"      \
  "License, or (at your option) any later version.\n\n"                       \
  "This program is distribute WITHOUT ANY WARRANTY; without even the\n"       \
  "implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE."

#define TITLE "cosimone's Chip-8 emulator"

#define CHIP8_HRES 64
#define CHIP8_VRES 32
#define INITIAL_WINDOW_HRES (CHIP8_HRES * 20)
#define INITIAL_WINDOW_VRES (CHIP8_VRES * 20)
#define CHIP8_BYTE_LENGTH 8

#define SAMPLE_RATE 44100
#define AMPLITUDE 6000
#define FREQUENCY (1 * 329.6279) /* E5 musical note */
#define INCREMENT (FREQUENCY / SAMPLE_RATE)

#define FRAMERATE 50
#define INSTS_PER_FRAME 50

#define FRAME_PERIOD_MILLI (1000 / FRAMERATE)

#define COLOR_ON 0x00FF0000u  /* RGBA, 8 bits per channel */
#define COLOR_OFF 0x00000000u /* RGBA, 8 bits per channel */

#define ARRAY_SIZE(a) (sizeof(a) / sizeof(a)[0])

struct display {
  SDL_Window   *window;
  SDL_Renderer *renderer;
  SDL_Texture  *texture;
};

struct video_memory {
  unsigned char pixels[CHIP8_VRES][CHIP8_HRES];
};

struct keyboard_memory {
  unsigned char keys[0x10];
};

struct speaker {
  SDL_AudioSpec spec;
  SDL_AudioSpec obtained;
  double        current_sample_value;
};

struct chip8 {
  struct display         display;
  struct speaker         speaker;
  struct video_memory    vmem;
  struct keyboard_memory kmem;

  unsigned long  start_time;
  unsigned short stack[0x10];
  unsigned short dt;
  unsigned short st;
  unsigned short I;
  unsigned short PC;
  unsigned char  V[16];
  unsigned char  ram[0x1000];
  unsigned char  SP;
  unsigned char  wait_flag;
  unsigned char  termination_flag;
};

static unsigned long get_current_time(unsigned long scale)
{
  struct timespec t;
  clock_gettime(CLOCK_REALTIME, &t);
  return t.tv_sec * scale + (t.tv_nsec / (1000000000L / scale));
}

static unsigned long get_current_time_msecs(void)
{
  return get_current_time(1000ul);
}

static void speaker_init(struct speaker *s)
{
  if (SDL_InitSubSystem(SDL_INIT_AUDIO) < 0) {
    fprintf(stderr, "SDL error: %s\n", SDL_GetError());
    exit(EXIT_FAILURE);
  }

  s->current_sample_value = 0;
  s->spec.freq            = SAMPLE_RATE;
  s->spec.format          = AUDIO_S16SYS;
  s->spec.channels        = 0;
  s->spec.samples         = SAMPLE_RATE / 20; /* 0x05 secs latency */
  s->spec.callback        = NULL;

  if (SDL_OpenAudio(&s->spec, &s->obtained) < 0) {
    fprintf(stderr, "SDL error: %s\n", SDL_GetError());
    exit(EXIT_FAILURE);
  }
}

static void speaker_destroy(struct speaker *s)
{
  (void) s;
  SDL_CloseAudio();
  SDL_QuitSubSystem(SDL_INIT_AUDIO);
}

static void trigger_sound(struct speaker *sp, unsigned st)
{
  if (st == 0) {
    SDL_PauseAudio(1);
  } else {
    Sint16 samples[SAMPLE_RATE / 20];
    double pi = atan(1) * 4;
    double x  = sp->current_sample_value;
    size_t i  = 0;

    for (; i < ARRAY_SIZE(samples); ++i, x += INCREMENT)
      samples[i] = AMPLITUDE * sin(x * 2 * pi);
    sp->current_sample_value = x;
    SDL_QueueAudio(1, samples, sizeof(samples));
    SDL_PauseAudio(0);
  }
}

static void display_init(struct display *d)
{
  if (SDL_InitSubSystem(SDL_INIT_VIDEO) < 0) {
    fprintf(stderr, "SDL error: %s\n", SDL_GetError());
    exit(EXIT_FAILURE);
  }

  d->window = SDL_CreateWindow(TITLE, SDL_WINDOWPOS_UNDEFINED,
                               SDL_WINDOWPOS_UNDEFINED, INITIAL_WINDOW_HRES,
                               INITIAL_WINDOW_VRES, SDL_WINDOW_RESIZABLE);
  if (!d->window) {
    fprintf(stderr, "SDL error: %s\n", SDL_GetError());
    exit(EXIT_FAILURE);
  }

  d->renderer = SDL_CreateRenderer(d->window, -1, 0);
  if (!d->renderer) {
    fprintf(stderr, "SDL error: %s\n", SDL_GetError());
    exit(EXIT_FAILURE);
  }

  d->texture =
    SDL_CreateTexture(d->renderer, SDL_PIXELFORMAT_RGBA8888,
                      SDL_TEXTUREACCESS_STREAMING, CHIP8_HRES, CHIP8_VRES);
  if (!d->texture) {
    fprintf(stderr, "SDL error: %s\n", SDL_GetError());
    exit(EXIT_FAILURE);
  }
}

static void display_destroy(struct display *d)
{
  SDL_DestroyTexture(d->texture);
  SDL_DestroyRenderer(d->renderer);
  SDL_DestroyWindow(d->window);
  SDL_QuitSubSystem(SDL_INIT_VIDEO);
}

static void draw(struct display *d, const struct video_memory *vmem)
{
  Uint32 pixels[ARRAY_SIZE(vmem->pixels) * ARRAY_SIZE(vmem->pixels[0])];
  size_t h = ARRAY_SIZE(vmem->pixels);
  size_t w = ARRAY_SIZE(vmem->pixels[0]);
  size_t i, j;

  for (i = 0; i < h; ++i) {
    for (j = 0; j < w; ++j) {
      unsigned color = vmem->pixels[i][j] ? COLOR_ON : COLOR_OFF;
      size_t   idx   = i * w + j;
      pixels[idx]    = color;
    }
  }

  if (SDL_UpdateTexture(d->texture, NULL, pixels, sizeof(*pixels) * w) < 0) {
    fprintf(stderr, "SDL error: %s\n", SDL_GetError());
    exit(EXIT_FAILURE);
  }
  if (SDL_RenderCopy(d->renderer, d->texture, NULL, NULL) < 0) {
    fprintf(stderr, "SDL error: %s\n", SDL_GetError());
    exit(EXIT_FAILURE);
  }
  SDL_RenderPresent(d->renderer);
}

static void read_rom(struct chip8 *ch, const char *fname)
{
  FILE  *fp = fopen(fname, "rb");
  size_t rompos, ramlen;

  if (!fp) {
    fprintf(stderr, "Error while opening ROM file %s\n", fname);
    exit(EXIT_FAILURE);
  }

  rompos = ch->PC;
  ramlen = ARRAY_SIZE(ch->ram);

  rompos += fread(&ch->ram[rompos], sizeof(*ch->ram),
                  (ramlen - rompos) * sizeof(*ch->ram), fp);

  if (ferror(fp)) {
    fclose(fp);
    fprintf(stderr, "An error occurred while reading %s\n", fname);
    exit(EXIT_FAILURE);
  }

  if (!feof(fp)) {
    fclose(fp);
    fprintf(stderr, "Error: ROM file %s is too large\n", fname);
    exit(EXIT_FAILURE);
  }

  fclose(fp);
  memset(&ch->ram[rompos], 0, (ramlen - rompos) * sizeof(*ch->ram));
}

static void draw_instr(struct chip8 *ch, unsigned x, unsigned y, unsigned n)
{
  struct video_memory *vmem   = &ch->vmem;
  unsigned             erased = 0, i, j;

  for (i = 0; i < n; ++i) {
    unsigned current_byte = ch->ram[ch->I + i];

    for (j = CHIP8_BYTE_LENGTH; j-- > 0;) {
      unsigned       row    = (ch->V[y] + i) % CHIP8_VRES;
      unsigned       col    = (ch->V[x] + j) % CHIP8_HRES;
      unsigned char *oldbit = &vmem->pixels[row][col];
      unsigned       newbit = (current_byte & 0x1) ^ *oldbit;

      if (!newbit && *oldbit)
        erased = 1;
      *oldbit = newbit;
      current_byte >>= 1;
    }
  }
  ch->V[0xF] = erased;
}

static void keypress_instr(struct chip8 *ch, unsigned x)
{
  const struct keyboard_memory *kmem    = &ch->kmem;
  unsigned                      pressed = 0, i;

  for (i = 0; i < ARRAY_SIZE(kmem->keys); ++i) {
    if (kmem->keys[i]) {
      ch->V[x] = i;
      pressed  = 1;
    }
  }
  ch->wait_flag = !pressed;
  if (ch->wait_flag)
    ch->PC -= 2;
}

static void chip8_tick_cpu(struct chip8 *ch)
{
  const struct keyboard_memory *kmem = &ch->kmem;
  struct video_memory          *vmem = &ch->vmem;

  unsigned instr = (ch->ram[ch->PC] << 8) | ch->ram[ch->PC + 1];

  /* Every value which could be used by an instruction is defined.
     The terms are based on Cowgod's Chip-8 reference. */
  unsigned opcode = (instr >> 12) & 0xFu;
  unsigned x      = (instr >> 8) & 0xFu;
  unsigned y      = (instr >> 4) & 0xFu;
  unsigned n      = instr & 0xFu;
  unsigned kk     = instr & 0xFFu;
  unsigned nnn    = instr & 0xFFFu;

  ch->PC += 2;

  switch (opcode) {
  case 0x0:
    switch (nnn) {
    case 0x0E0:
      memset(vmem->pixels, 0, sizeof(vmem->pixels));
      break;
    case 0x0EE:
      ch->PC = ch->stack[ch->SP--];
      break;
    default:
      ch->PC = nnn; /* Legacy! */
      break;
    }
    break;
  case 0x1:
    ch->PC = nnn;
    break;
  case 0x2:
    ch->stack[++ch->SP] = ch->PC;
    ch->PC              = nnn;
    break;
  case 0x3:
    if (ch->V[x] == kk)
      ch->PC += 2;
    break;
  case 0x4:
    if (ch->V[x] != kk)
      ch->PC += 2;
    break;
  case 0x5:
    if (ch->V[x] == ch->V[y])
      ch->PC += 2;
    break;
  case 0x6:
    ch->V[x] = kk;
    break;
  case 0x7:
    ch->V[x] += kk;
    break;
  case 0x8:
    switch (n) {
    case 0x0:
      ch->V[x] = ch->V[y];
      break;
    case 0x1:
      ch->V[x] |= ch->V[y];
      break;
    case 0x2:
      ch->V[x] &= ch->V[y];
      break;
    case 0x3:
      ch->V[x] ^= ch->V[y];
      break;
    case 0x4: {
      unsigned sum = ch->V[x] + ch->V[y];
      ch->V[0xF]   = sum > 255;
      ch->V[x]     = sum & 0xFF;
      break;
    }
    case 0x5:
      ch->V[0xF] = ch->V[x] > ch->V[y];
      ch->V[x] -= ch->V[y];
      break;
    case 0x6:
      ch->V[0xF] = ch->V[x] & 0x1;
      ch->V[x] >>= 1;
      break;
    case 0x7:
      ch->V[0xF] = ch->V[y] > ch->V[x];
      ch->V[x]   = (unsigned) ch->V[y] - (unsigned) ch->V[x];
      break;
    case 0xE:
      ch->V[0xF] = ch->V[x] >> 7;
      ch->V[x] <<= 1;
      break;
    default:
      fprintf(stderr, "Error: invalid instruction: 0x%x\n", instr);
      exit(EXIT_FAILURE);
      break;
    }
    break;
  case 0x9:
    if (ch->V[x] != ch->V[y])
      ch->PC += 2;
    break;
  case 0xA:
    ch->I = nnn;
    break;
  case 0xB:
    ch->PC = nnn + ch->V[0x0];
    break;
  case 0xC:
    ch->V[x] = rand() & kk;
    break;
  case 0xD:
    draw_instr(ch, x, y, n);
    break;
  case 0xE:
    switch (kk) {
    case 0x9E:
      if (kmem->keys[ch->V[x]])
        ch->PC += 2;
      break;
    case 0xA1:
      if (!kmem->keys[ch->V[x]])
        ch->PC += 2;
      break;
    default:
      fprintf(stderr, "Error: invalid instruction: 0x%x\n", instr);
      exit(EXIT_FAILURE);
      break;
    }
    break;
  case 0xF:
    switch (kk) {
    case 0x07:
      ch->V[x] = ch->dt;
      break;
    case 0x0A:
      keypress_instr(ch, x);
      break;
    case 0x15:
      ch->dt = ch->V[x];
      break;
    case 0x18:
      ch->st = ch->V[x];
      break;
    case 0x1E:
      ch->I += ch->V[x];
      break;
    case 0x29:
      ch->I = ch->V[x] * 5;
      break;
    case 0x33:
      ch->ram[ch->I]     = (ch->V[x] / 100) % 10;
      ch->ram[ch->I + 1] = (ch->V[x] / 10) % 10;
      ch->ram[ch->I + 2] = ch->V[x] % 10;
      break;
    case 0x55:
      memcpy(&ch->ram[ch->I], ch->V, sizeof(*ch->V) * (x + 1));
      ch->I += x + 1;
      break;
    case 0x65:
      memcpy(ch->V, &ch->ram[ch->I], sizeof(*ch->ram) * (x + 1));
      ch->I += x + 1;
      break;
    default:
      fprintf(stderr, "Error: invalid instruction: 0x%x\n", instr);
      exit(EXIT_FAILURE);
      break;
    }
    break;
  default:
    fprintf(stderr, "Error: invalid instruction: 0x%x\n", instr);
    exit(EXIT_FAILURE);
    break;
  }
}

static void chip8_init(struct chip8 *ch, const char *rom_filename)
{
  static const unsigned char fontset[] = {
    0xF0, 0x90, 0x90, 0x90, 0xF0, /* 0 */
    0x20, 0x60, 0x20, 0x20, 0x70, /* 1 */
    0xF0, 0x10, 0xF0, 0x80, 0xF0, /* 2 */
    0xF0, 0x10, 0xF0, 0x10, 0xF0, /* 3 */
    0x90, 0x90, 0xF0, 0x10, 0x10, /* 4 */
    0xF0, 0x80, 0xF0, 0x10, 0xF0, /* 5 */
    0xF0, 0x80, 0xF0, 0x90, 0xF0, /* 6 */
    0xF0, 0x10, 0x20, 0x40, 0x40, /* 7 */
    0xF0, 0x90, 0xF0, 0x90, 0xF0, /* 8 */
    0xF0, 0x90, 0xF0, 0x10, 0xF0, /* 9 */
    0xF0, 0x90, 0xF0, 0x90, 0x90, /* A */
    0xE0, 0x90, 0xE0, 0x90, 0xE0, /* B */
    0xF0, 0x80, 0x80, 0x80, 0xF0, /* C */
    0xE0, 0x90, 0x90, 0x90, 0xE0, /* D */
    0xF0, 0x80, 0xF0, 0x80, 0xF0, /* E */
    0xF0, 0x80, 0xF0, 0x80, 0x80  /* F */
  };

  ch->start_time       = get_current_time_msecs();
  ch->dt               = 0;
  ch->st               = 0;
  ch->wait_flag        = 0;
  ch->termination_flag = 0;
  ch->SP               = 0x0;
  ch->I                = 0x0;
  ch->PC               = 0x200;

  memset(ch->vmem.pixels, 0, sizeof(ch->vmem.pixels));
  memset(ch->kmem.keys, 0, sizeof(ch->kmem.keys));
  memset(ch->V, 0, sizeof(ch->V));
  memset(ch->stack, 0, sizeof(ch->stack));
  memcpy(ch->ram, fontset, sizeof(fontset));

  read_rom(ch, rom_filename);

  if (SDL_InitSubSystem(SDL_INIT_EVENTS) < 0) {
    fprintf(stderr, "SDL error: %s\n", SDL_GetError());
    exit(EXIT_FAILURE);
  }
  display_init(&ch->display);
  speaker_init(&ch->speaker);

  srand(time(NULL));
}

static void chip8_destroy(struct chip8 *ch)
{
  display_destroy(&ch->display);
  speaker_destroy(&ch->speaker);
  SDL_QuitSubSystem(SDL_INIT_EVENTS);
  SDL_Quit();
}

static void handle_events(struct chip8 *ch)
{
  int keymap[] = {SDLK_x, SDLK_1, SDLK_2, SDLK_3, SDLK_q, SDLK_w,
                  SDLK_e, SDLK_a, SDLK_s, SDLK_d, SDLK_z, SDLK_c,
                  SDLK_4, SDLK_r, SDLK_f, SDLK_v};

  SDL_Event ev;

  while (SDL_PollEvent(&ev)) {
    switch (ev.type) {
      unsigned i;
      int      key;
    case SDL_QUIT:
      ch->termination_flag = 1;
      break;
    case SDL_KEYDOWN:
    case SDL_KEYUP:
      key = ev.key.keysym.sym;
      for (i = 0; i < ARRAY_SIZE(keymap); ++i) {
        if (keymap[i] == key) {
          ch->kmem.keys[i] = ev.type == SDL_KEYDOWN;
          break;
        }
      }
      break;
    default:
      break;
    }
  }
}

static void chip8_execute_cycles(struct chip8 *ch)
{
  unsigned long now              = get_current_time_msecs();
  unsigned long delta            = now - ch->start_time;
  unsigned      frames_to_render = delta / FRAME_PERIOD_MILLI;

  if (delta >= FRAME_PERIOD_MILLI)
    ch->start_time = now;
  if (frames_to_render == 0)
    usleep(FRAME_PERIOD_MILLI * 1000);

  while (frames_to_render--) {
    unsigned nticks = INSTS_PER_FRAME;

    if (ch->st > 0)
      --ch->st;
    if (ch->dt > 0)
      --ch->dt;
    while (nticks-- && !ch->wait_flag)
      chip8_tick_cpu(ch);
    handle_events(ch);
    trigger_sound(&ch->speaker, ch->st);
    draw(&ch->display, &ch->vmem);
    ch->wait_flag = 0;
  }
}

int main(int argc, char *argv[])
{
  struct chip8 ch;

  if (argc != 2) {
    fprintf(stderr, "Use as %s <filename>\n", argv[0]);
    return EXIT_FAILURE;
  }
  if (!strcmp(argv[1], "-v") || !strcmp(argv[1], "--version")) {
    puts(LICENSE_INFO);
    return 0;
  }
  chip8_init(&ch, argv[1]);
  while (!ch.termination_flag)
    chip8_execute_cycles(&ch);
  chip8_destroy(&ch);
  return 0;
}
