# Chip-8 emulator/interpreter

My implementation of a Chip-8 emulator.  Multimedia (graphics, audio and
input) are handled via the popular SDL library, version 2.

The emulator has been developed according to Cowgod's specifications.
(http://devernay.free.fr/hacks/chip8/C8TECH10.HTM)

The source code is strictly C89 compliant, we only make use of some POSIX
features for timing purposes.

## Build

### Unix-like systems

Simply runun `make`. The Makefile provided uses `gcc` as the default
compiler. It's possible to create a debug build using `make debug`. This is a
slightly modified version optimized for debuggers and it is not
intended for normal use.

## Usage
`./chip8 <romfilename>`

The Chip-8 keyboard is structured as follows:

![Chip8 keyboard](keyboard.gif)

The keys are mapped, from left to right and from top to bottom, to {1,
2, 3, 4}, {Q, W, E, R}, {A, S, D, F}, {Z, X, C, V}.
