CC = gcc
CFLAGS = -std=c89 -O3  -pedantic-errors -Wall -Wextra -Wpedantic -DNDEBUG
LIBS = -lm -lSDL2
CHIP8 = chip8
TAGS = etags
TAGFILE = TAGS

OBJS = $(CHIP8).o

all: $(CHIP8) tags

debug: CFLAGS := $(CFLAGS) -Og -g -UNDEBUG
debug: all

clean:
	rm -rf $(CHIP8) $(OBJS) $(TAGFILE)

$(CHIP8): $(CHIP8).o
	$(CC) $(CFLAGS) $(CHIP8).o $(LIBS) -o $(CHIP8)

$(CHIP8).o: $(CHIP8).c
	$(CC) $(CFLAGS) -c $(CHIP8).c -o $(CHIP8).o

tags:
	$(TAGS) $(CHIP8).c -o $(TAGFILE)
